//
//  UIView+Common.m
//  MarketMap
//
//  Created by Yuriy Romanchenko on 11/1/14.
//  Copyright (c) 2014 Roman Ivchenko. All rights reserved.
//

#import "UIView+Common.h"

@implementation UIView (Common)

#pragma mark - Dynamic Properties

- (UIViewCoordinates)coordinates {
    return UIViewCoordinatesFromCGRect(self.frame);
}

- (void)setX:(CGFloat)x {
    CGRect newRect = self.frame;
    
    newRect.origin.x = x;
    
    self.frame = newRect;
}

- (CGFloat)x {
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y {
    CGRect newRect = self.frame;
    
    newRect.origin.y = y;
    
    self.frame = newRect;
}

- (CGFloat)y {
    return self.frame.origin.y;
}

- (void)setWidth:(CGFloat)width {
    CGRect newRect = self.frame;
    
    newRect.size.width = width;
    
    self.frame = newRect;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height {
    CGRect newRect = self.frame;
    
    newRect.size.height = height;
    
    self.frame = newRect;
}

- (CGFloat)height {
    return self.frame.size.height;
}

#pragma mark - Public

- (void)setFrame:(CGRect)frame restoreCenter:(BOOL)restore {
    CGPoint currentCenter = self.center;
    
    self.frame = frame;
    
    self.center = currentCenter;
}

#pragma mark - Additional Helpers

UIViewCoordinates UIViewCoordinatesFromCGRect(CGRect viewRect) {
    // Assume that we have a square, not a quadliteral.
    CGPoint topLeft = viewRect.origin;
    CGPoint topRight = (CGPoint){topLeft.x + CGRectGetWidth(viewRect), topLeft.y};
    CGPoint bottomLeft = (CGPoint){topLeft.x, topLeft.y + CGRectGetHeight(viewRect)};
    CGPoint bottomRight = (CGPoint){topRight.x, bottomLeft.y};
    
    return (UIViewCoordinates){topLeft, topRight, bottomLeft, bottomRight};
}

NSString *NSStringFromUIViewCoordinates(UIViewCoordinates coordinates) {
    return [NSString stringWithFormat:@"TL: %@ TR: %@ BL: %@BR: %@", NSStringFromCGPoint(coordinates.topLeft),
                                                                     NSStringFromCGPoint(coordinates.topRight),
                                                                     NSStringFromCGPoint(coordinates.bottomLeft),
                                                                     NSStringFromCGPoint(coordinates.bottomRight)];
}


@end
