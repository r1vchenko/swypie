//
//  UIView+Common.h
//  MarketMap
//
//  Created by Yuriy Romanchenko on 11/1/14.
//  Copyright (c) 2014 Roman Ivchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef struct {
    CGPoint topLeft;
    CGPoint topRight;
    CGPoint bottomLeft;
    CGPoint bottomRight;
} UIViewCoordinates;

@interface UIView (Common)

@property (nonatomic, readonly) UIViewCoordinates coordinates;

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

/** This method acts as regular setFrame except it saves view center. Useful for scaling. */
- (void)setFrame:(CGRect)frame restoreCenter:(BOOL)restore;

// TODO: Move to another category (Geometry e.g.)
UIViewCoordinates UIViewCoordinatesFromCGRect(CGRect viewRect);
NSString *NSStringFromUIViewCoordinates(UIViewCoordinates coordinates);

@end
