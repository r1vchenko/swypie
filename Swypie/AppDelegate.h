//
//  AppDelegate.h
//  Swypie
//
//  Created by Roman Ivchenko on 3/30/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

