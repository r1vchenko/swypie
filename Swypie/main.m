//
//  main.m
//  Swypie
//
//  Created by Roman Ivchenko on 3/30/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
