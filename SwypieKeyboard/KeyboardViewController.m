//
//  KeyboardViewController.m
//  SwypieKeyboard
//
//  Created by Roman Ivchenko on 3/30/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import "KeyboardViewController.h"

// Categories
#import "UIView+Common.h"

// Model
#import "Vertex.h"
#import "SimpleBrokenLine.h"

@interface KeyboardViewController ()

@end

@implementation KeyboardViewController {
    UIView *_keyboardContainer;
    
    IBOutletCollection(UIButton) NSArray *keyboardButtons;
    
    BOOL _capitalizingEnabled;
    NSMutableArray *_vertexes;
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    // Add custom view sizing constraints here
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _vertexes = [NSMutableArray new];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

#pragma mark - UITextInput

- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {

}

#pragma mark - Private

- (void)loadInterface {
    // load the nib file
    UINib *keyboardNib = [UINib nibWithNibName:@"KeyboardViewController"
                                        bundle:nil];
    // instantiate the view
    _keyboardContainer = [[keyboardNib instantiateWithOwner:self options:nil] firstObject];
    
    [self.view addSubview:_keyboardContainer];
}

#pragma mark - Callbacks

- (IBAction)didSwypeKeyboard:(UIPanGestureRecognizer *)sender {
    Vertex *vertex = [Vertex new];
    
    vertex.x = [sender locationInView:sender.view].x;
    vertex.y = [sender locationInView:sender.view].y;
    
    [_vertexes addObject:vertex];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self calculateVertexes];
    }
}

- (IBAction)backspaceDidTap:(UIButton *)sender {
    [self.textDocumentProxy deleteBackward];
}

- (IBAction)capitalizeDidTap:(UIButton *)sender {
    _capitalizingEnabled = !_capitalizingEnabled;
}

- (IBAction)changeKeyboardDidTap:(UIButton *)sender {
    //required functionality, switches to user's next keyboard
    [self advanceToNextInputMode];
}

- (IBAction)spaceDidTap:(UIButton *)sender {
    [self.textDocumentProxy insertText:@" "];
}

- (IBAction)returnDidTap:(UIButton *)sender {
    [self.textDocumentProxy insertText:@"\n"];
}

- (IBAction)keyboardLetterButtonDidTap:(UIButton *)sender {
    NSString *letter = _capitalizingEnabled ? [sender.titleLabel.text uppercaseString] : [sender.titleLabel.text lowercaseString];
    [self.textDocumentProxy insertText:letter];
}

#pragma mark - Private

- (void)calculateVertexes {
//    NSMutableArray *arr = [NSMutableArray new];
//    for (NSInteger i = 0; i < [_vertexes count]; i++) {
//        NSInteger secondVertexIndex = i + 2;
//        if (secondVertexIndex > [_vertexes count]) {
//            secondVertexIndex = i;
//        }
//        SimpleBrokenLine *smpLine = [[SimpleBrokenLine alloc] initWithVertexes:[_vertexes subarrayWithRange:NSMakeRange(i,secondVertexIndex)]];
//        [arr addObject:smpLine];
//    }
    NSLog(@"YOYO");
}

@end
