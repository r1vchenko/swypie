//
//  KeyboardViewController.h
//  SwypieKeyboard
//
//  Created by Roman Ivchenko on 3/30/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardViewController : UIInputViewController

@end
