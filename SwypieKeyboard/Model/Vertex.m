//
//  Vertex.m
//  Swypie
//
//  Created by Roman Ivchenko on 3/31/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import "Vertex.h"

@implementation Vertex

#pragma mark - Overriden

- (NSString *)description {
    return [NSString stringWithFormat:@"X:%.1f Y:%.1f", _x, _y];
}

@end
