//
//  Edge.m
//  Swypie
//
//  Created by Roman Ivchenko on 3/31/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import "SimpleBrokenLine.h"

@implementation SimpleBrokenLine {
    NSArray *_vertexes;
}

#pragma mark - Init

- (instancetype)initWithVertexes:(NSMutableArray *)vertexes {
    if (self = [super init]) {
        _vertexes = vertexes;
        
        
    }
    return self;
}

#pragma mark - Overriden

- (NSString *)description {
    return [_vertexes count] == 3 ? [NSString stringWithFormat:@"Vertexes: %@ %@ %@", [_vertexes[0] description], [_vertexes[1] description], [_vertexes[2] description]] : [NSString stringWithFormat:@"Vertexes: %@ %@", [_vertexes[0] description], [_vertexes[1] description]];
}

@end
