//
//  Vertex.h
//  Swypie
//
//  Created by Roman Ivchenko on 3/31/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kVertexWeightHigh,
    kVertexWeightMedium,
    kVertexWeightLow
} VertexWeight;

@interface Vertex : NSObject

@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;

@property (nonatomic, assign) float associatedAngle;
@property (nonatomic, assign) VertexWeight weight;

@end
