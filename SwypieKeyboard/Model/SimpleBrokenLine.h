//
//  Edge.h
//  Swypie
//
//  Created by Roman Ivchenko on 3/31/15.
//  Copyright (c) 2015 Micro-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleBrokenLine : NSObject

- (instancetype)initWithVertexes:(NSArray *)vertexes;

@end
